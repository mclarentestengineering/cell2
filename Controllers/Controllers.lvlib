﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Controls" Type="Folder">
		<Item Name="Control Settings.ctl" Type="VI" URL="../Controls/Control Settings.ctl"/>
	</Item>
	<Item Name="Compare Setpoint Profiles.vi" Type="VI" URL="../Compare Setpoint Profiles.vi"/>
	<Item Name="Configure Analog Control Task.vi" Type="VI" URL="../Configure Analog Control Task.vi"/>
	<Item Name="Configure Digital Control Task.vi" Type="VI" URL="../Configure Digital Control Task.vi"/>
	<Item Name="Controllers message loop.vi" Type="VI" URL="../Controllers message loop.vi"/>
	<Item Name="Create Tasks.vi" Type="VI" URL="../Create Tasks.vi"/>
	<Item Name="Display Current Test Info.vi" Type="VI" URL="../../Display/Display Current Test Info.vi"/>
	<Item Name="Setpoint Profile Viewer.vi" Type="VI" URL="../Setpoint Profile Viewer.vi"/>
	<Item Name="Setup Control Tasks.vi" Type="VI" URL="../Setup Control Tasks.vi"/>
	<Item Name="Stop Control Tasks.vi" Type="VI" URL="../Stop Control Tasks.vi"/>
	<Item Name="Stop Test.vi" Type="VI" URL="../Stop Test.vi"/>
</Library>
